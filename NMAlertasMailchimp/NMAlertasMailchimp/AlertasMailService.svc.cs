﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using NMAlertasMailchimp.Conexion;
using System.Data;
using System.Data.SqlClient;
using Oracle.DataAccess.Client;
using System.Configuration;
using System.IO;

using System.ServiceModel.Web;
using System.Web.Script.Serialization;
using System.Globalization;
namespace NMAlertasMailchimp
{
    public class AlertasMailService : IAlertasMailService
    {
        private List<ResponseData> repo;
        public AlertasMailService()
        {
            repo = new List<ResponseData>();
        }
        public List<ResponseData> registrar(RequestData data)
        {
            string cod = null;
            string msg = null;
            NMOracleParameter objNMOracleParameter = new NMOracleParameter();


            try
            {
                using (OracleConnection objConnection = new OracleConnection(DataConexion.strCnx_appWebsOracle))
                {

                    objConnection.Open();

                    using (OracleCommand objCommand = new OracleCommand())
                    {
                        objCommand.CommandText = DataConexion.strUsuario_appWebsOracle + ".PKG_MNT_ALERTAS.SP_REGISTRAR_ALERTA";
                        objCommand.CommandType = CommandType.StoredProcedure;
                        objCommand.Connection = objConnection;

                        objNMOracleParameter.AddParameter(objCommand, "pclicot_email", OracleDbType.Varchar2, data.email, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "pclicot_email_alter", OracleDbType.Varchar2, null, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "pclicot_fec_crea", OracleDbType.Varchar2, null, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "pclicot_fec_upd", OracleDbType.Varchar2, null, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "pclicot_recibir_promo", OracleDbType.Varchar2, null, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "pruc", OracleDbType.Varchar2, null, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "prazon_social", OracleDbType.Varchar2, null, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "pcontrasena", OracleDbType.Varchar2, null, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "pidsesion", OracleDbType.Varchar2, null, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "pdni", OracleDbType.Varchar2, null, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "ppasaporte", OracleDbType.Varchar2, null, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "ppais_int_dpto", OracleDbType.Varchar2, null, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "pred_social", OracleDbType.Varchar2, data.plataforma, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "pes_persona", OracleDbType.Varchar2,null, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "pidsession_redsocial", OracleDbType.Varchar2, null, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "pfoto_perfil", OracleDbType.Blob, null, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "pcod_vinculado", OracleDbType.Varchar2, null, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "pflg_oferta", OracleDbType.Varchar2, data.flgoferta, ParameterDirection.Input);
                        objNMOracleParameter.AddParameter(objCommand, "pcuenta_activa", OracleDbType.Varchar2, data.ctaactiva, ParameterDirection.Input);

                        objNMOracleParameter.AddParameter(objCommand, "pcodigo", OracleDbType.Varchar2, null, ParameterDirection.Output, 200);
                        objNMOracleParameter.AddParameter(objCommand, "pmensaje", OracleDbType.Varchar2, null, ParameterDirection.Output, 200);

                        objCommand.ExecuteNonQuery();
                        //cod = Convert.ToInt32(objCommand.Parameters["pCodigo"].Value.ToString());
                        cod = objCommand.Parameters["pcodigo"].Value.ToString();
                        msg = objCommand.Parameters["pmensaje"].Value.ToString();

                        repo.Add(new ResponseData()
                        {
                            mensaje = msg,
                            codigo = cod
                        });

                    }
                }
            }
            catch (Exception ex)
            {

                // throw new Exception(ex.ToString());
                repo.Add(new ResponseData()
                {
                    mensaje = "error",
                    codigo = "-1"
                });

            }

            return repo;
        }
        public string Get(int id)
        {
            return "Test OK";
        }
    }
}
