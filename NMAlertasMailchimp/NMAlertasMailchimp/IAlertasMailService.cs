﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace NMAlertasMailchimp
{
    [DataContract]
    public class RequestData
    {
        [DataMember]
        public string email { get; set; }
        [DataMember]
        public string plataforma { get; set; }
        [DataMember]
        public string flgoferta { get; set; }
        [DataMember]
        public string ctaactiva { get; set; }

    }

    public class RequestData2
    {

        [DataMember]
        public string cid { get; set; }

    }
    public class ResponseData
    {
        [DataMember]
        public string mensaje { get; set; }
        [DataMember]
        public string codigo { get; set; }
    }
    [ServiceContract]
    public interface IAlertasMailService
    {
        [OperationContract]
        [WebInvoke(UriTemplate = "/alertasMailChimp/registrarAlertaMail",
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        List<ResponseData> registrar(RequestData data);

        [OperationContract]
        [WebGet(UriTemplate = "/alertasMailChimp/get?id={id}",
            ResponseFormat = WebMessageFormat.Json,
            RequestFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare)]
        string Get(int id);
    }
}
