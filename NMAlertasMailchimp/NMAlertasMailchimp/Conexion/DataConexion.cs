﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NMAlertasMailchimp.Conexion
{
    public class DataConexion
    {
        #region Oracle - BD WEBS
        private const string strNomServ_WebsOracle = "(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = 10.75.102.16)(PORT = 1521))(CONNECT_DATA =(SERVER = DEDICATED)(SERVICE_NAME = ORCL)))";
        public const string strUsuario_WebsOracle = "APPENGINE";
        private const string strClave_WebsOracle = "\"appengine\"";

        public const string strCnx_WebsOracle = "Data Source=" + strNomServ_WebsOracle +
                                                ";User ID=" + strUsuario_WebsOracle +
                                                ";Password=" + strClave_WebsOracle;

        //appWebs  
        private const string strNomServ_appWebsOracle = "(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = 10.75.102.15)(PORT = 1521))(CONNECT_DATA =(SERVER = DEDICATED)(SERVICE_NAME = ORCL)))";
        public const string strUsuario_appWebsOracle = "appwebs";
        private const string strClave_appWebsOracle = "6109338";

        public const string strCnx_appWebsOracle = "Data Source=" + strNomServ_appWebsOracle +
                                                ";User ID=" + strUsuario_appWebsOracle +
                                                ";Password=" + strClave_appWebsOracle;
        #endregion

    }
}